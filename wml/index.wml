#use wml::templ::template title="" author="Raphaël Hertzog, Martin Michlmayr, Peter Palfrader, Josip Rodin, Colin Watson, Luk Claes, Jeroen van Wolffelaar, Christoph Berg, Lucas Nussbaum, and others"

{#style#:
<link type="text/css" rel="stylesheet" href="debian.css" />
:##}

<div id="second-nav">
<p>Links</p>
<ul>
 <li><a href="https://wiki.debian.org/qa.debian.org"><b>qa.debian.org Wiki</b></a></li>
 <li><a href="https://wiki.debian.org/qa.debian.org/Join">How to join and help</a></li>
 <li><a href="https://wnpp.debian.net/" title="Work-Needing and Prospective Packages">Packages to work on (WNPP)</a></li>
 <li><a href="https://qa.debian.org/debcheck.php" title="Package relationships check">Debcheck</a></li>
 <li><a href="https://qa.debian.org/daca" title="Debian's Automated Code Analysis project">DACA</a></li>
</ul>
</div>

<p>The Debian Quality Assurance (QA) Team tries to improve the distribution as a whole, not only a specific set of packages. It also serves as a central place for discussion about distribution-wide problems and improvements regarding quality.

<p>Some tasks we are doing/have done:
<ul>
<li>Coordination of mass bug filings and transitions
<li>Tracking of maintainers <a href="https://wiki.debian.org/qa.debian.org/MIATeam">Missing In Action (MIA)</a>
<li>Development of tools such as the <a href="https://tracker.debian.org">Debian Package Tracker</a> and the <a href="https://qa.debian.org/developer.php">Developer's Packages Overview</a>
<li>Constantly running systematic checks on the entire archive and publishing the results at <a href="https://piuparts.debian.org">piuparts.debian.org</a> and <a href="https://lintian.debian.org">lintian.debian.org</a>. These results are then also displayed in the PTS.
<li>Mass-bug filing about various problems (of severity important or higher), either detected by piuparts/lintian or by rebuilding the whole archive 
<li>Emergency maintenance of orphaned packages
</ul>

<p>Most of the interesting stuff is on the <a href="https://wiki.debian.org/qa.debian.org">qa.debian.org Wiki pages</a>.

<p>If you are interested, don't hesitate to <a href="https://wiki.debian.org/qa.debian.org/Join">join us</a>, whether you are a Debian Developer or not. There's stuff to do for everybody!
