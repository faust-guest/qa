#!/bin/sh

# We don't want translated Descriptions here
LC_MESSAGES=C
export LC_MESSAGES

pkg_list=''
REASON="mia"

usage ()
  {
    echo "Usage: $0 [-r reason] [-b bcc-adress] [-h|--help] pkg1 [pkg2] ... [pkgN]"
    echo ""
    echo "    -h print this help"
    echo "    -b specify the BCC address yourself instead of generate it with mia-*@qa.debian.org"
    echo "    -c specify a X-DEBBUGS-CC address instead of using the maintainer's address"
    echo "    -r give the reason of orphaning (orphan, mia, rfa or retired)"
    echo "    -m specify what to use as name of the maintainer in the body of the mail."
    echo "    -a specify an addtional text to be added to the email body."
    echo ""
    exit 1
  }

if [ "$#" -eq 0 ] ; then
  usage
fi

while [ "$#" != "0" ] ; do
  case "$1" in
    -b)
      BCCADD=$2
      shift
    ;;
    -c)
      DEBBUGSCC=$2
      shift
    ;;
    -r)
      if [ "$2" = "mia" -o "$2" = "orphan" -o "$2" = "retired" -o "$2" = "rfa" ] ; then
       REASON=$2
       shift
      else
       usage
      fi
    ;;
    -m)
     MAINTNAME=$2
     shift
    ;;
    -a)
     ADDTEXT="$2"
     shift
    ;;
    -h|--help|-*) usage ;;
    *)
        pkg_list="$pkg_list $1"
    ;;
  esac
  shift
done

orphan () {
PKG="$1"
BIN="`apt-cache showsrc "$PKG" | grep ^Binary | sed -e 's/,//g' | cut -d' ' -f 2-`"
MAINT="`apt-cache showsrc "$PKG" | grep ^Maintainer | cut -d' ' -f 2- | uniq`"
if [ -z "${MAINTNAME:-}" ] ; then  MAINTNAME=$MAINT; fi

DEBBUGSCC=${DEBBUGSCC:-"$MAINT"}
DESCR="`apt-cache show $BIN | grep ^Description | head -n 1 | cut -d' ' -f 2-`"
TAG="`echo $MAINT | sed -e 's/.*<\(.*\)>.*/\1/' -e 's/@debian.org$//' -e 's/@/=/'`"

if [ ! "$BIN" ] || [ ! "$MAINT" ] || [ ! "$DESCR" ] || [ ! "$TAG" ] ; then
  echo "Could not fetch all relevant package data for $PKG" 1>&2
  exit 1
fi

BUGTYPE="O"

case "$REASON" in
  orphan) reason_text="has orphaned this package.";;
  mia) reason_text="is apparently not active anymore.  Therefore, I orphan this package now.";;
  retired) reason_text="has retired.  Therefore, I orphan this package now.";;
  rfa) reason_text="looks for someone to take over this package."; BUGTYPE="RFA" ;;
esac

# add line breaks to $ADDTEXT
if [ "$ADDTEXT" ] ; then
ADDTEXT="
$ADDTEXT
"
fi

(
cat <<EOF
Package: wnpp

The current maintainer of $PKG, $MAINTNAME,
$reason_text

Maintaining a package requires time and skills. Please only adopt this
package if you will have enough time and attention to work on it.
$ADDTEXT
If you want to be the new maintainer, please see
https://www.debian.org/devel/wnpp/#howto-o for detailed
instructions how to adopt a package properly.

Some information about this package:

EOF
apt-cache showsrc "$PKG"
apt-cache show $BIN
) > orphan-mail

apt-cache showsrc "$PKG" > /dev/null

if [ ! $BCCADD ] ; then
  BCCADD=mia-$TAG@qa.debian.org
fi

if [ $! ] ; then
  exit 1
else
  mutt -s "$BUGTYPE: $PKG -- $DESCR" \
       -e "my_hdr X-Debbugs-Cc: $DEBBUGSCC" \
       -e "my_hdr X-MIA-Summary: -\\; orphaning $PKG" \
       -e "set autoedit" \
       -b "$BCCADD" \
       -i orphan-mail \
       submit@bugs.debian.org
fi
rm -f orphan-mail
}

for PKG in $pkg_list ; do
	orphan "$PKG"
done

# vim:sw=2
