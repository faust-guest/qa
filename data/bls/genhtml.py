#!/usr/bin/python3
# Copyright 2011,2012,2013 Bernhard R. Link <brlink@debian.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# SOFTWARE IN THE PUBLIC INTEREST, INC. BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

import datetime, os, re
# use codecs module so this also works with python2
# (once that is no longer needed, do s/codecs.open/open/
import codecs

class utc(datetime.tzinfo):
	def utcoffset(self, dt):
		return datetime.timedelta(0)
	def dst(self, dt):
		return datetime.timedelta(0)
def stamp2date(stamp):
	return datetime.datetime.fromtimestamp(int(stamp), utc()).__format__("%Y-%m-%d %H:%M")


vars = dict()
if os.path.exists("settings.conf"):
	f = codecs.open("settings.conf", "r", encoding="utf-8")
	for l in f:
		if len(l) == 0 or l.startswith('#'):
			continue
		sep = l.find("='")
		if l[0].isspace() or l[-2:] != "'\n" or sep <= 0:
			raise Exception("Malformed settings.conf line %s, must be NAME='value'" % repr(l))
		vars[l[:sep]] = l[sep+2:-2]
	f.close()
bld = vars.get('BUILDLOGFETCHURL', 'https://buildd.debian.org/status/fetch.php?')
VARDIR = vars.get('VARDIR', 'var')
DATABASE = vars.get('DATABASE', 'service=qa-buildlogchecks') #VARDIR + "/cache.sqlite")
uploaders = vars.get('UPLOADERS', VARDIR + "/Uploaders")
graphdir = vars.get('GRAPHDIR', "graphs")
htmldir = vars.get('HTMLDIR', "/srv/qa.debian.org/web/bls.new")
templatedir = vars.get('TEMPLATEDIR', "/srv/qa.debian.org/data/bls/templates")
descriptionsdir = vars.get('DESCRIPTIONDIR', "/srv/qa.debian.org/data/bls/descriptions")
DATERUN = vars.get('DATERUN', datetime.datetime.now(utc()).__format__("%Y-%m-%d %H:%M UTC"))
doexport = vars.get('EXPORT', "no") == "yes"

if not os.path.exists(htmldir):
	os.mkdir(htmldir)
if not os.path.exists(htmldir + "/byuploader"):
	os.mkdir(htmldir + "/byuploader")
if not os.path.exists(htmldir + "/bytag"):
	os.mkdir(htmldir + "/bytag")
if not os.path.exists(htmldir + "/packages"):
	os.mkdir(htmldir + "/packages")
if not os.path.exists(uploaders):
	raise Exception("Uploaders file %s does not exit. Please download it from mirror/debian/indices/ or symlink it to a up-to-date copy" % uploaders)

dbparms = dict()
if DATABASE.startswith("service=") or DATABASE.startswith("database="):
	DIST = vars.get('DIST', "sid")
	dbparms["logs"] = vars.get('logstable', "pub.logs_" + DIST)
	dbparms["tags"] = vars.get('tagstable', "pub.tags_" + DIST)
	dbparms["stats"] = vars.get('statstable', "pub.stats_" + DIST)
	dbparms["?"] = "%s"

	import psycopg2

	conn = psycopg2.connect(DATABASE)
else:
	dbparms["logs"] = vars.get('logstable', "logs")
	dbparms["tags"] = vars.get('tagstable', "tags")
	dbparms["?"] = "?"

	import sqlite3
	if not os.path.exists(DATABASE):
		raise Exception("results database file %s does not exit!" % DATABASE)

	conn = sqlite3.connect(DATABASE)

notscanned = dict()
def getnotscanned(id):
	if id in notscanned:
		return notscanned[id]
	cursor = conn.cursor()
	cursor.execute('SELECT COUNT(*) FROM {logs} WHERE checkdone < {?}'.format(**dbparms), [id])
	data = cursor.fetchone()[0]
	notscanned[id] = data
	return data

def getall(cmd, params):
	cursor = conn.cursor()
	cursor.execute(cmd.format(**dbparms), params)
	return cursor.fetchall()

lastcheck = int(getall("SELECT max(checkdone) FROM {logs}", [])[0][0])

html_escape_map = {ord('&'): '&amp;', ord('<'): '&lt;', ord('>'): '&gt;',
			ord('"'): '&quot;', ord('\''): '&#x27;'}
url_escape_map = dict(
                        map(lambda i: (i, "%%%02x" %i),
                        filter(lambda i: i < ord('-') or i == ord('/') or (i > ord('9') and i < ord('A')) or (i > ord('Z') and i < ord('_')) or (i > ord('_') and i < ord('a')) or i > ord('z'),
                        range(256))))
def urlargescape(s):
	return s.translate(url_escape_map)
def urlescape(s):
	return s.translate(url_escape_map)
def htmlescape(s):
	return s.translate(html_escape_map)

templates = dict()
includere = re.compile('INCLUDE\(([a-z+.-]+)(,TITLE=([^)]*))?\)\n?')

def includeinclude(x):
	title = x.group(3)
	include = readtemplate(x.group(1), ".inc")
	if title:
		return include.replace("TITLE", x.group(3))
	else:
		return include

def readtemplate(template, suffix=".html"):
	if template + suffix in templates:
		return templates[template+suffix]
	f = codecs.open(templatedir + "/" + template + suffix, "r", encoding="utf-8")
	contents = f.read()
	f.close()
	contents = includere.sub(includeinclude, contents)
	templates[template+suffix] = contents
	return contents

def backdir(target):
	return '../' * (len(target.split("/"))-1)

def instantiatetemplate(template, target, *commands, **replacements):
	replacements["DATERUN"] = DATERUN
	replacements["BASEDIR/"] = backdir(target)
	replacements["GRAPHDIR/"] = backdir(target) + graphdir + "/"
	rc = re.compile('\\b(' + '|'.join(map(re.escape, replacements))+")\\b\n?")
	contents = rc.sub(lambda x: replacements[x.group(1)], readtemplate(template))
	f = codecs.open(htmldir + "/" + target + ".html", "w", encoding="utf-8")
	f.write(contents)
	f.close()

descriptions = dict()
for d in filter(lambda x: len(x) > 12 and x[-12:]==".description", os.listdir(descriptionsdir)):
	name = d[:-12]

	df = codecs.open(descriptionsdir + "/" + d, "r", encoding="utf-8")
	lines = df.readlines()
	df.close()
	descr = "".join(filter(lambda x: x[0:2] != '##', lines))
	for s in filter(lambda x: x[0:10] == '## Since: ', lines):
		missing = getnotscanned(int(s[10:]))
		if missing:
			descr = descr + '<br><p class="notice">This is a new tag, there are %d logs not yet scanned for it.</p>\n' % missing
	tags = getall("SELECT DISTINCT package FROM {tags} WHERE tag = {?} ORDER BY package", [name])
	descriptions[name] = len(tags)
	packages = "".join(map(lambda x: '<li><a href="../packages/%s/%s.html">%s</a></li>\n' % (urlescape(x[0][0:1]),urlescape(x[0]), htmlescape(x[0])), tags))
	instantiatetemplate("bytag", "bytag/" + name, TAGNAME=name, DESCRIPTION=descr, PACKAGES=packages)

# per start letter index.html files

for (l,) in sorted(getall("SELECT DISTINCT substr(package,1,1) FROM {logs}", [])):
	if not os.path.exists(htmldir + "/packages/" + l):
		os.mkdir(htmldir + "/packages/" + l)
	packages = "".join(map(lambda x: '<li><a href="%s.html">%s</a></li>\n' % (urlescape(x[0]), htmlescape(x[0])), getall("SELECT DISTINCT package FROM {tags} WHERE substr(package,1,1) = {?} ORDER BY package", [l])))
	instantiatetemplate("byletter", "packages/%s/index" % l,
			STARTLETTER=l, PACKAGES=packages)

# per package pages:
class Collector(object):
	def __iter__(self):
		return self
	def getlogs(self):
		for p, da, dv, ds, dc in self.logs:
			if p != self.logpkg:
				r = self.logdata
				self.logpkg = p
				self.logdata = [(da,dv,ds,dc)]
				return r
			else:
				self.logdata.append((da,dv,ds,dc))
		r = self.logdata
		self.logpkg = None
		self.logdata = None
		return r
	def __init__(self, pkgs, tags):
		self.logs = iter(pkgs)
		self.tags = iter(tags)
		l = next(self.logs, [None])
		self.logpkg = l[0]
		self.logdata = [l[1:]]
		self.tagpkg = None
		self.taglist = []
		self.tagname = None
		self.tagdata = None
		self.tagarchlist = []
	def replacetag(self, tagname, tagdata, dl):
		if self.tagname:
			self.taglist.append((self.tagname, self.tagdata, self.tagarchlist))
		self.tagname = tagname
		self.tagdata = tagdata
		self.tagarchlist = dl
	def replacecurrent(self, tagpkg, tagname, tagdata, dl):
		self.replacetag(tagname, tagdata, dl)
		if self.tagpkg:
			if self.tagpkg == self.logpkg:
				r = (self.tagpkg, self.getlogs(), self.taglist)
			else:
				r = (self.tagpkg, [], self.taglist)
				raise Exception("Found package '%s' in tags but no in logs!" % self.tagpkg)
		else:
			r = None
		self.tagpkg = tagpkg
		self.taglist = []
		return r
	def issuefreepkg(self):
		p = self.logpkg
		r = (p, self.getlogs(), [])
		return r
	def __next__(self):
		# handle packages without issues:
		if self.tagpkg and self.logpkg and self.logpkg < self.tagpkg:
			return self.issuefreepkg()
		# find the next package with tags combined:
		for tagpkg, tagname, tagdata, da, dv, ds in self.tags:
			if tagdata is None:
				tagdata = ""
			if tagpkg == self.tagpkg:
				# collect tag data while still in the same package:
				if self.tagname == tagname and self.tagdata == tagdata:
					self.tagarchlist.append((da,dv,ds))
				else:
					self.replacetag(tagname, tagdata, [(da,dv,ds)])
			else:
				# return the old package, start a new one:
				r = self.replacecurrent(tagpkg, tagname, tagdata, [(da,dv,ds)])
				if r != None:
					return r
				# handle packages without issues:
				if self.logpkg and self.logpkg < self.tagpkg:
					return self.issuefreepkg()
		# if there is no new data, throw out all there is:
		r = self.replacecurrent(None, None, None, [])
		if r:
			return r
		if self.logpkg:
			return self.issuefreepkg()
		raise StopIteration()
	next = __next__

for package, loglist, taglist in Collector(getall("SELECT package, architecture, version, stamp, checkdone FROM {logs} ORDER by package, architecture", []),getall("SELECT package, tag, data, architecture, version, stamp FROM {tags} ORDER BY package, tag, data, architecture", [])):
	# per package files for packages with issues:
	if len(loglist) == 0:
		print("Unexpected package name in tag information: %s" % package)
		continue
	# information which logs were already scanned
	logdata = ""
	loghint = ""
	for arch, version, stamp, check in loglist:
		linkdata="%spkg=%s&arch=%s&ver=%s&stamp=%s&raw=0" % (bld, urlargescape(package),
			urlargescape(arch), urlargescape(version), urlargescape(stamp))
		if int(check) < lastcheck:
			ch = "<sup>" + (lastcheck - int(check))* "*" + "</sup>"
			loghint = True
		else:
			ch = ""
		logdata = logdata + '<li>%s %s <a href="%s">%s</a>%s</li>\n' % (
					htmlescape(arch), htmlescape(version),
					htmlescape(linkdata),
					htmlescape(stamp2date(stamp)),
					ch)
	if loghint:
		loghint = "<sup>*</sup>: Not yet scanned with the most recent scanner version."
	if len(taglist) == 0:
		#  files for packages without issues (to avoid dead links)
		instantiatetemplate("noissues", "packages/" + package[0] + "/" + package, PACKAGENAME=package, LOGS=logdata, LOGHINT=loghint)
		continue
	# tag information:
	count = 0
	data = ""
	for (tagname, tagdata, archlist) in taglist:
		if len(tagname) < 3 or tagname[1] != '-':
			print("Strange tag name '%s'" % tagname)
			continue
		tagtype = htmlescape(tagname[0])
		tagshortname = htmlescape(tagname[2:])
		if not os.path.exists(htmldir + "/bytag/" + tagname + ".html"):
			data = data + '<li>%s %s (' % (htmlescape(tagname), htmlescape(tagdata))
			print("Unexpected tagname '%s' in package '%s'!" % (tagname, package))
		else:
			data = data + '<li><span class="type-%s">%s</span> <a href="../../bytag/%s.html">%s</a> %s (' % (tagtype, tagtype, urlescape(tagname), tagshortname, htmlescape(tagdata))
		count = count + 1
		pref = ''
		for (arch, version, stamp) in archlist:
			linkdata="%spkg=%s&arch=%s&ver=%s&stamp=%s&raw=0" % (bld, urlargescape(package),
				urlargescape(arch), urlargescape(version), urlargescape(stamp))
			data = data + pref + '<a href="%s">%s</a>' % (htmlescape(linkdata), htmlescape(arch))
			pref = ', '
		data = data + ')</li>\n'
	instantiatetemplate("package", "packages/" + package[0] + "/" + package, PACKAGENAME=package, TAGS=data, ISSUECOUNT=str(count), LOGS=logdata, LOGHINT=loghint)

# generate bymaintainer files:

if "stats" in dbparms:
	data = getall("SELECT package, errors, warnings, infos, experimental FROM {stats} ORDER BY package", [])
else:
	data = getall("SELECT package, COUNT(CASE substr(tag,1,1) WHEN 'E' THEN tag ELSE NULL END) AS errors, COUNT(CASE substr(tag,1,1) WHEN 'W' THEN tag ELSE NULL END) AS warnings, COUNT(CASE substr(tag,1,1) WHEN 'I' THEN tag ELSE NULL END) AS infos, COUNT(CASE substr(tag,1,1) WHEN 'X' THEN tag ELSE NULL END) AS experimental FROM (SELECT package, tag, COUNT(1) AS count FROM {tags} GROUP by package,tag) AS stats WHERE tag != 'W-build-stamp-in-binary' OR count > 3 GROUP BY package", [])

f = codecs.open(uploaders, "r", encoding="utf-8")
uploaders = dict()
for l in f.readlines():
	x = l.split(None, 1)
	if len(x) < 2:
		continue
	pkg = x[0]
	maint = x[1]
	if not pkg in uploaders:
		uploaders[pkg] = [maint]
	else:
		uploaders[pkg].append(maint)
f.close()

class Uploader(object):
	def __init__(self, name, email):
		self.name = name
		self.email = email
		self.packages = []
	normalized = dict()
	byemail = dict()
	bystartletter = dict()

uploaderrc = re.compile('^(, |"|)([^<>@]*[^"<>@])"? *<([a-z0-9A-Z][^<>&@]*)@([^&<>@]+)>[, \\t\\n]*$')
def uploader_add_package(package, uploader):
	if uploader in Uploader.normalized:
		Uploader.normalized[uploader].packages.append(package)
		return
	x = uploaderrc.match(uploader)
	if not x:
		# FIXME: remove this check when this bug is fixed:
		#        https://bugs.debian.org/868101
		if uploader.strip() != 'None':
			print("Cannot parse ", uploader)
		return
	name = x.group(2)
	email = (x.group(3)+'_'+x.group(4)).lower()
	if email in Uploader.byemail:
		Uploader.byemail[email].packages.append(package)
		Uploader.normalized[uploader] = Uploader.byemail[email]
		return
	u = Uploader(name, email)
	u.packages.append(package)
	if email[0] in Uploader.bystartletter:
		Uploader.bystartletter[email[0]].append(email)
	else:
		Uploader.bystartletter[email[0]] = [email]
	Uploader.byemail[email] = u
	Uploader.normalized[uploader] =  u

for d in data:
	if not str(d[0]) in uploaders:
		continue
	for u in uploaders[str(d[0])]:
		uploader_add_package(d, u)

for l in sorted(Uploader.bystartletter.keys()):
	result = []
	for e in sorted(Uploader.bystartletter[l]):
		u = Uploader.byemail[e]
		li = '<h3 class="uploader"><a name="%s"></a>%s &lt;%s&gt;</h3><table class="packages"><tr><th>package</th><th class="type-E">E</th><th class="type-W">W</th><th class="type-I">I</th><th class="type-X">X</th></tr>\n' % (htmlescape(u.email),htmlescape(u.name),htmlescape(u.email))
		for pkg in u.packages:
			li = li + ('<tr><td><a href="../packages/%s/%s.html">%s</a></td><td>%d</td><td>%d</td><td>%d</td><td>%d</td></tr>\n' % (
				urlescape(pkg[0][0:1]), urlescape(pkg[0]), htmlescape(pkg[0]), pkg[1], pkg[2], pkg[3], pkg[4]))
		li = li + '</table>'
		result.append(li)
	instantiatetemplate("byuploader", "byuploader/%s" % l, DATA="\n".join(result),LETTER=l)

for l in [ 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' ]:
	if l not in Uploader.bystartletter:
		instantiatetemplate("byuploader", "byuploader/%s" % l, DATA="Nothing found",LETTER=l)

# generate index.html:

packages = "".join(map(lambda x: '<li><a href="packages/%s/index.html">%s</a></li>\n' % (urlescape(x[0]),htmlescape(x[0])),
		sorted(getall("SELECT DISTINCT substr(package,1,1) FROM {tags}", []))))
tagsraw = sorted(descriptions.keys())
tagsraw = list(filter(lambda x: x[0] =='E', tagsraw)) + list(filter(lambda x: x[0] =='W', tagsraw)) + list(filter(lambda x: x[0] =='I', tagsraw)) + list(filter(lambda x: x[0] !='E' and x[0] != 'W' and x[0] != 'I', tagsraw))
tags = "".join(map(lambda x: '<li><span class="type-%s">%s</span> <a href="bytag/%s.html">%s</a> %d</li>\n' % (htmlescape(x[0]), htmlescape(x[0]), urlescape(x), htmlescape(x[2:]), descriptions[x]), tagsraw))
uploaders = "".join(map(lambda x: '<li><a href="byuploader/%s.html">%s</a></li>\n' % (urlescape(x[0]),htmlescape(x[0])),
		sorted(Uploader.bystartletter.keys())))
instantiatetemplate("index", "index", TAGS=tags, PACKAGES=packages, UPLOADERS=uploaders)

# generate logcheck.txt:

data ="".join(map(lambda x: "|".join(map(str,x))+"\n", data))
f = codecs.open(htmldir + "/logcheck.txt", "w", encoding="utf-8")
f.write(data)
f.close()

if doexport:
	os.system("cp templates/bls.css '" + htmldir + "'/bls.css")
	if not(DATABASE.startswith("service=") or DATABASE.startswith("database=")):
		os.system("gzip -c '" + DATABASE +"' > '" + htmldir + "'/cache.gz")
	os.system("git bundle create '" + htmldir + "'/gitbundle master")
