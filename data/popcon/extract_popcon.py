#!/usr/bin/python
#  Copyright (C) 2004 Igor Genibel                                         
#  Copyright (C) 2005, 2006, 2008, 2009 Christoph Berg <myon@debian.org>
#                                                                          
#  This program is free software; you can redistribute it and/or           
#  modify it under the terms of the GNU General Public License             
#  as published by the Free Software Foundation; either version 2          
#  of the License, or (at your option) any later version.                  
#                                                                          
#  This program is distributed in the hope that it will be useful,         
#  but WITHOUT ANY WARRANTY; without even the implied warranty of          
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           
#  GNU General Public License for more details.                            

# This extracts the information from popcon for each package

import sys, os, psycopg2, re

verbose = sys.argv.__len__() > 1

pg = psycopg2.connect('service=qa')
cur = pg.cursor()
cur.execute("SET search_path TO ddpo")

def process_popcon ():
    popcon = open("by_inst", 'rb')
    reg1 = re.compile('^(\d+)\s+(\S+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)')

    (max_inst, max_vote, max_old, max_recent, max_no_files) = (0, 0, 0, 0, 0)

    while 1:
        line = popcon.readline()
        try:
            line = line.decode('utf-8').strip()
        except UnicodeDecodeError:
            continue
        if not line:
            break
        match = reg1.search(line)
        if not match:
            continue
        (rank, binary, inst, vote, old, recent, no_files) = \
                (match.group(1), match.group(2),
                 int(match.group(3)), int(match.group(4)), int(match.group(5)),
                 int(match.group(6)), int(match.group(7)))
        if binary == 'Total':
            continue

        if inst > max_inst: max_inst = inst
        if vote > max_vote: max_vote = vote
        if old > max_old: max_old = old
        if recent > max_recent: max_recent = recent
        if no_files > max_no_files: max_no_files = no_files

        cur.execute("""INSERT INTO popcon
                       VALUES (%s, %s, %s, %s, %s, %s)""",
                       [binary, inst, vote, old, recent, no_files])

    cur.execute("""INSERT INTO popcon
                   VALUES ('', %s, %s, %s, %s, %s)""",
                   [max_inst, max_vote, max_old, max_recent, max_no_files])

if __name__ == "__main__":
    cur.execute("BEGIN")
    cur.execute("""TRUNCATE popcon""")
    process_popcon()
    cur.execute("COMMIT")

# vim:et:sw=4:
